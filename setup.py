import setuptools

with open("README", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='haspirater',
    version='0.2',
    author="Antoine Amarilli",
    author_email="a3nm@a3nm.net",
    package_data={'haspirater' :['*json']},
    description="detect aspirated 'h' in French words",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/a3nm/haspirater",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
)
